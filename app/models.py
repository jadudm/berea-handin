from peewee import *
from config import cfg
import datetime

class BaseModel (Model):
    class Meta:
      database = cfg.db

class ConstantsModel (Model):
    class Meta:
      database = cfg.constants

class Semesters (ConstantsModel):
  sid       = PrimaryKeyField()
  name      = TextField()

class Years (ConstantsModel):
  yid       = PrimaryKeyField()
  year      = IntegerField()

class Assignment (BaseModel):
  aid       = PrimaryKeyField()
  name      = TextField()
  dueDate   = DateTimeField()
  descURL   = TextField()
  semester  = ForeignKeyField(Semesters)
  year      = ForeignKeyField(Years)
  # Regexp? Allowed extensions?
  # Max file size?

class Submission (BaseModel):
  sid       = PrimaryKeyField()
  aid       = ForeignKeyField(Assignment, related_name = "submissions")
  filename  = TextField()
  date      = DateTimeField( default = datetime.datetime.now)
