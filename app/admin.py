from flask_admin import Admin
from flask_admin.contrib.peewee import ModelView

# ModelViews that we want to override/extend.
class YearModelView (ModelView):
  column_list = ["yid", "year"]

# Set up the admin interface
from models import *
from flask_admin.contrib.peewee import ModelView
from app import app

admin = Admin(app, name = "handin", template_mode = "bootstrap3")
admin.add_view(ModelView(Semesters))
admin.add_view(YearModelView(Years))
