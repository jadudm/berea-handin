virtualenv env

. env/bin/activate

pip install flask

# For Forms
pip install flask-wtf

# For Models
#pip install peewee
git clone --depth=1 https://github.com/coleifer/peewee.git
pushd peewee
  python setup.py install
popd
rm -rf peewee

# For Admin
# pip install flask-admin
git clone --depth=1 git@github.com:flask-admin/flask-admin.git
pushd flask-admin
  python setup.py install
popd
rm -rf flask-admin

pip install wtf-peewee
