# Laying out a Flask App

https://www.digitalocean.com/community/tutorials/how-to-structure-large-flask-applications

# Template
https://github.com/berlotto/flask-app-template

# Productionizing
https://www.jeffknupp.com/blog/2014/01/29/productionizing-a-flask-application/

# Extensions
http://flask.pocoo.org/extensions/

# Peewee
http://docs.peewee-orm.com/en/latest/peewee/quickstart.html
