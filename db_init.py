from app.models import *
from config import cfg

cfg.db.connect()

try:
  if not Semesters.table_exists():
    Semesters.create_table()
  if not Years.table_exists():
    Years.create_table()
except Exception as inst:
  print type(inst)
  print inst.args
