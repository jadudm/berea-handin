from app import app

if __name__ == '__main__':
  import logging
  FORMAT = '%(asctime)-15s %(message)s'
  logger = logging.getLogger(__name__)
  logging.basicConfig(format=FORMAT)
  logging.getLogger().setLevel(logging.DEBUG)

  app.run(host  = '0.0.0.0', \
          port  = 8080, \
          debug = True
          )
